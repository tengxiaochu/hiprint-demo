import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

createApp(App).use(router).mount('#app')

// import ElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'


